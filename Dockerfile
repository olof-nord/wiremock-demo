FROM openjdk:8-jre

WORKDIR /home/wiremock

ENV WIREMOCK_VERSION 2.27.1

# download the Wiremock standalone JAR
RUN wget "https://repo1.maven.org/maven2/com/github/tomakehurst/wiremock-jre8-standalone/${WIREMOCK_VERSION}/wiremock-jre8-standalone-${WIREMOCK_VERSION}.jar" \
    -O wiremock-jre8-standalone.jar

COPY resources/__files __files
COPY resources/mappings mappings

EXPOSE 8080
ENTRYPOINT exec java -jar wiremock-jre8-standalone.jar --root-dir /home/wiremock --port 8080 --verbose --global-response-templating
